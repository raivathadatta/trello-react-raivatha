import { Card, CircularProgress,Container } from "@mui/material";
import FormInput from "./FormInput";
import { useEffect, useState } from "react";
import { getAllCardsFromListId } from "../../../data/getData/get_api_calls";
import { deleteCardById } from "../../../data/delete/delete_api_calls";
import { createCard } from "../../../data/create/create_api_calls";
import CheckListDialogBox from "../dialogs/CheckItemListDialogBox";
import ErrorSnackBar from "../snackbar/ErrorSnackBar";

function CardContainer({ listId }) {
  const [cardDetails, setCardDetails] = useState([]);
  const [errorMessage, setErrorMessage] = useState("");
  const [loading, setLoading] = useState(false);

  const handleError = (message) => {
    setErrorMessage({ message });
  };

  const getCards = async () => {
    setLoading(true);
    const response = await getAllCardsFromListId(listId);
    if (response.error) {
      handleError(response.error);
      return;
    }
    setCardDetails(response.data);
    setLoading(false);
  };

  useEffect(() => {
    getCards();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const addNewCard = async (cardTitle) => {
    const response = await createCard(listId, cardTitle);
    if (response.error) {
      handleError(response.error);
      return;
    }
    setCardDetails([...cardDetails, response.data]);
  };

  const deleteCard = async (cardId) => {
    const response = await deleteCardById(cardId);
    if (response.error) {
      handleError(response.error);
      return;
    }

    const newCard = cardDetails.filter((card) => card.id !== cardId);
    setCardDetails(newCard);
  };

  if (errorMessage) {
    return <ErrorSnackBar errorMessage={errorMessage}   onCloseError={()=>setErrorMessage('')} />;
  }

  if (loading) {
    return (
      <Container sx={{ display: "flex" }}>
        <CircularProgress />
      </Container>
    );
  }

  return (
    <>
      {cardDetails.map((card) => {
        return (
          <Card key={card.id} sx={{ marginTop: 2, width: "280px" }}>
            <CheckListDialogBox
              deleteCardCallBack={() => deleteCard(card.id)}
              name={card.name}
              cardId={card.id}
            ></CheckListDialogBox>
          </Card>
        );
      })}

      <FormInput
        inputKey={listId}
       
        onSubmit={addNewCard}
        buttonText="+Add New Card"
        placeholder="New Card Tittle"
      ></FormInput>
    </>
  );
}

export default CardContainer;
