import { Box, TextField, Button, Typography, IconButton } from "@mui/material";
import { useState } from "react";
import CloseIcon from "@mui/icons-material/Close";

function FormInput({
  inputKey,
  onSubmit,
  buttonText,
  placeholder,
}) {
  const [isVisible, setIsVisible] = useState(false);
  const [inputValue, setInputValue] = useState("");

  const handleSubmitCallBack = (event) => {
    event.preventDefault();
    toggleAddListInput();
    onSubmit(inputValue);
    setInputValue("");
  };
  const handleCross = () => {
    setInputValue("");
    toggleAddListInput();
  };

  const toggleAddListInput = () => {
    setIsVisible(!isVisible);
  };
  const handleChange = (event) => {
    setInputValue(event.target.value);
  };
  return (
    <>
      {isVisible ? (
        <Box
          component="Form"
          sx={{
            marginTop: 3,
            boxShadow: 2,
            width: "250px",
            marginBottom: 3,
            borderRadius: 2,
            backgroundColor: "white",
            padding: "2%",
          }}
          onSubmit={handleSubmitCallBack}
        >
          <TextField
            key={inputKey}
            autoFocus
            variant="standard"
            value={inputValue}
            onChange={handleChange}
            placeholder={placeholder}
            InputProps={{ disableUnderline: true }}
            sx={{
              background: "white",
              outline: "none",
              borderRadius: 1,
            }}
          ></TextField>

          <Box
            sx={{
              display: "flex",
              alignItems: "center",
            }}
          >
            <Box>
              <Button
                variant="contained"
                sx={{
                  background: "primary.main",
                  color: "white",
                  textTransform: "none",
                  borderRadius: 10,
                  width: "100px",
                }}
                onClick={handleSubmitCallBack}
              >
                <Typography>Add </Typography>
              </Button>
            </Box>
            <Box>
              <IconButton
                onClick={handleCross}
                sx={{
                  color: "red",
                  borderRadius: 1,
                  padding: 1,
                }}
              >
                <CloseIcon></CloseIcon>
              </IconButton>
            </Box>
          </Box>
        </Box>
      ) : (
        <Box
          sx={{
            width: "250px",
            background: "white",
            padding: 2,
            borderRadius: 2,
            boxShadow: 1,
            cursor: "pointer",
            margin: 1,
          }}
        >
          <Typography variant="body2" onClick={toggleAddListInput}>
            {buttonText}
          </Typography>
        </Box>
      )}
    </>
  );
}

export default FormInput;
