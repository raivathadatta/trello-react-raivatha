import { useState } from "react";

import { Snackbar,Alert } from "@mui/material";

export default function ErrorSnackBar({ errorMessage,onCloseError }) {
  const [open, setOpen] = useState(true);
  const handleClose=()=>{
    setOpen(false)
    onCloseError()
  }
  return (

    <>
      <Snackbar
        open={open}
        autoHideDuration={4000}
        onClose={handleClose}
        anchorOrigin={{ vertical: "top", horizontal: "center" }}
      >
        <Alert severity="error" variant="filled">
          {errorMessage}
        </Alert>
      </Snackbar>
    </>
  );
}
