import { MdOutlineDelete } from "react-icons/md";
import {
  IconButton,
  Box,
  Typography,
  Dialog,
  Container,
  DialogTitle,
} from "@mui/material";
import { useState } from "react";
import { getCheckList } from "../../../data/getData/get_api_calls";
import FormInput from "../form/FormInput";
import { deleteCheckListById } from "../../../data/delete/delete_api_calls";
import { createCheckList } from "../../../data/create/create_api_calls";
import CheckListBox from "../containers/CheckListBox";
import CircularProgress from "@mui/material/CircularProgress";
import ErrorSnackBar from "../snackbar/ErrorSnackBar";

function CheckListDialogBox({ deleteCardCallBack, name, cardId }) {
  const [open, setOpen] = useState(false);
  const [checkList, setChecklist] = useState([]);
  const [loading, setLoading] = useState(false);

  const [errorMessage, setErrorMessage] = useState("");

  const handleError = (message) => {
    setErrorMessage({ message });
  };

  const onClickDeleteCard = () => {
    deleteCardCallBack();
  };

  const showDialogBox = () => {
    getCheckItemList();
  };
  const handleClose = () => {
    setOpen(false);
  };

  const onSubmit = async (tittle) => {
    const response = await createCheckList(cardId, tittle);
    if (response.error) {
      handleError(response.error);
      return;
    }

    setChecklist([...checkList, response.data]);
  };

  const deleteCheckList = async (checklistId) => {
    const response = await deleteCheckListById(checklistId.id);
    if (response.error) {
      console.log(response.error, "error");
      return;
    }
    const newCheckList = checkList.filter(
      (checklist) => checklist.id !== checklistId.id
    );
    setChecklist(newCheckList);
  };

  /// get checkList

  const getCheckItemList = async () => {
    setLoading(true);
    const response = await getCheckList(cardId);

    if (response.error) {
      setErrorMessage(response.error);
    }
    setChecklist(response.data);
    setOpen(true);
    setLoading(false);
  };

  if (errorMessage) {
    return <ErrorSnackBar errorMessage={errorMessage}  onCloseError={()=>setErrorMessage('')} />;
  }

  if (loading) {
    return (
      <Box sx={{ display: "flex", justifyContent: "center" }}>
        <CircularProgress />
      </Box>
    );
  }

  return (
    <>
      <Box
        sx={{
          display: "flex",
          alignItems: "center",
          justifyContent: "space-between",
        }}
      >
        <Box
          sx={{
            padding: 1,
            display: "flex",
            alignItems: "center",
            justifyContent: "space-between",
            width: "80%",
          }}
          onClick={showDialogBox}
        >
          <Typography>{name}</Typography>
        </Box>
        <IconButton aria-label="delete" onClick={onClickDeleteCard}>
          <MdOutlineDelete />
        </IconButton>
      </Box>

      <Dialog open={open} onClose={handleClose}>
        <DialogTitle>
          <Box>
            <Typography>{name}</Typography>
          </Box>
        </DialogTitle>

        <Container
          sx={{
            width: "500px",
            display: "flex",
            flexDirection: "column",
            alignItems: "center",
            backgroundColor: "lightgrey",
            justifyItems: "center",
            padding: 1,
            borderRadius: 1,
            boxShadow: 2,
          }}
        >
          <Container sx={{ height: "fit-content" }}>
            {loading ? (
              <Box
                sx={{ minHeight: "500px", marginTop: "40%", marginLeft: "40%" }}
              >
                <CircularProgress />
              </Box>
            ) : (
              checkList.map((item, index) => {
                return (
                  <CheckListBox
                    key={item.id}
                    item={item}
                    deleteCallBack={() => deleteCheckList(item)}
                    cardId={cardId}
                  />
                );
              })
            )}
          </Container>

          <FormInput
            inputKey={cardId}
            onSubmit={onSubmit}
            buttonText="+ Add New checkList"
            placeholder="New CheckList"
          ></FormInput>
        </Container>
      </Dialog>
    </>
  );
}
export default CheckListDialogBox;
