import List from "@mui/material/List";
import ListItem from "@mui/material/ListItem";
import ListItemButton from "@mui/material/ListItemButton";
import ListItemIcon from "@mui/material/ListItemIcon";
import ListItemText from "@mui/material/ListItemText";
import Checkbox from "@mui/material/Checkbox";
import IconButton from "@mui/material/IconButton";
import Container from "@mui/material/Container";
import { Box } from "@mui/material";
import { MdOutlineDelete } from "react-icons/md";
import { useEffect, useState } from "react";
import { deleteCheckList } from "../../../data/delete/delete_api_calls";
import { getCheckItemByListId } from "../../../data/getData/get_api_calls";
import { upDateCheckListStatus } from "../../../data/update/update_api_calls";
import { createCheckItem } from "../../../data/create/create_api_calls";
import FormInput from "../form/FormInput";
import ErrorSnackBar from "../snackbar/ErrorSnackBar";
import LinearWithValueLabel from "../progressBars/LinearProgressBar";

function ListContainer({ listId, cardId }) {
  const [list, setList] = useState([]);
  const [errorMessage, setErrorMessage] = useState("");
  // const [checkedCount, setCheckedCount] = useState(0);

  const handleError = (message) => {
    setErrorMessage(message);
  };

  const handleToggle = async (checkListId, state) => {
    const status = state == "incomplete" ? "complete" : "incomplete";
    let response = await upDateCheckListStatus(
      cardId,
      listId,
      checkListId,
      status
    );
    if (response.error) {
      handleError(response.error);
      return;
    }

    const newList = list.map((item) => {
      if (item.id == response.data.id) {
        item.state = response.data.state;
        return item;
      }
      return item;
    });
    setList(newList);
  };

  const addNewCheckList = async (title) => {
    let response = await createCheckItem(listId, title);
    if (response.error) {
      handleError(response.error);
      return;
    }
    setList([...list, response.data]);
  };

  const deleteCheckItemList = async (checkItemId) => {
    let response = await deleteCheckList(checkItemId, listId);
    if (response.error) {
      handleError(response.error);
      return;
    }
    const newList = list.filter((checkItem) => checkItem.id !== checkItemId);
    setList(newList);
  };

  const getCheckItemList = async () => {
    const response = await getCheckItemByListId(listId);
    if (response.error) {
      handleError(response.error);
      return;
    }
    setList(response.data);
  };
  useEffect(() => {
    getCheckItemList();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
  if (errorMessage) {
    return <ErrorSnackBar errorMessage={errorMessage}   onCloseError={()=>setErrorMessage('')}/>;
  }

  const checkedCount = list.filter(
    (value) => value.state === "complete"
  ).length;

  return (
    <>
      <Container sx={{ background: "white", boxShadow: 2, border: 1 }}>
        <List
          sx={{ width: "100%", maxWidth: 360, bgcolor: "background.paper" }}
        >
          <LinearWithValueLabel
            value={
              list.length == 0
                ? 0
                : Math.ceil((checkedCount / list.length) * 100)
            }
          />
          {list
            ? list.map((value) => {
                const labelId = `checkbox-list-label-${value}`;

                return (
                  <ListItem
                    key={value}
                    sx={{
                      margin: 2,
                      border: 2,
                      textDecoration:
                        value.state === "complete" ? "line-through" : "none",
                    }}
                    secondaryAction={
                      <IconButton
                        edge="end"
                        aria-label="delete"
                        onClick={() => deleteCheckItemList(value.id)}
                      >
                        <MdOutlineDelete />
                      </IconButton>
                    }
                    disablePadding
                  >
                    <ListItemButton
                      onClick={() => handleToggle(value.id, value.state)}
                      dense
                    >
                      <ListItemIcon>
                        <Checkbox
                          edge="start"
                          checked={value.state == "incomplete" ? false : true}
                          tabIndex={-1}
                          disableRipple
                          inputProps={{ "aria-labelledby": labelId }}
                        />
                      </ListItemIcon>
                      <ListItemText id={labelId} primary={value.name} />
                    </ListItemButton>
                  </ListItem>
                );
              })
            : ""}

          <Box>
            <FormInput
              inputKey={`${cardId}` + `${listId}` + `${125}`}
            
              onSubmit={addNewCheckList}
              placeholder="Add new CheckItem"
              buttonText="+ Add new CheckItem"
            ></FormInput>
          </Box>
        </List>
      </Container>
    </>
  );
}

export default ListContainer;
