import { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { getAllListsFromBoard } from "../data/getData/get_api_calls";
import ListScreenAppBar from "./components/appbars/ListScreenAppBar";
import { Box, Container } from "@mui/material";
import { createNewList } from "../data/create/create_api_calls";
import FormInput from "./components/form/FormInput";
import { deleteListByListId } from "../data/delete/delete_api_calls";
import ListCardContainer from "./components/containers/ListCardContainer";
import CircularProgress from "@mui/material/CircularProgress";
import ErrorSnackBar from "./components/snackbar/ErrorSnackBar";

function ListScreen() {
  let { boardId } = useParams();
  const [list, setList] = useState([]);
  const [loader, setLoader] = useState(true);
  const [errorMessage, setErrorMessage] = useState("");
  const handleError = (message) => {
    console.log(message);
    setErrorMessage(message);
  };

  const getLists = async () => {
    const response = await getAllListsFromBoard(boardId);
    if (response.error) {
      handleError(response.error);

      return;
    }

    setList(response.data);
    setLoader(false);
  };
  useEffect(() => {
    getLists();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const addNewList = async (listTittle) => {
    const response = await createNewList(listTittle, boardId);
    if (response.error) {
      handleError(response.error);
      return;
    }
    setList([...list, response.data]);
  };

  const deleteList = async (listId) => {
    const response = await deleteListByListId(listId);
    if (response.error) {
      handleError();
      return;
    }
    const listDataCopy = list.filter((item) => item.id !== listId);
    setList(listDataCopy);
  };

  if (errorMessage) {
 
   return <ErrorSnackBar errorMessage={errorMessage} onCloseError={()=>setErrorMessage('')}/>
    
  }
  if (loader) {
    return (
      <Container sx={{ marginTop: "20%", marginLeft: "45%" }}>
        <CircularProgress color="success" />;
      </Container>
    );
  }

  return (
    <>
      <ListScreenAppBar />

      <view
        style={{
          marginTop: 3,
          overflowX: "auto",
          display: "flex",
          width: "100vw",
          height: "100vh",
        }}
      >
        {list.map((list) => (
          <ListCardContainer
            key={list.id}
            list={list}
            deleteListCallBack={() => deleteList(list.id)}
          ></ListCardContainer>
        ))}

        <Box
          sx={{
            backgroundColor: "lightgrey",
            cursor: "pointer",
            justifyContent: "center",
            minWidth: "300px",
            height: "fit-content",

            marginTop: 3,
            borderRadius: 2,
          }}
        >
          <FormInput
            inputKey={"addList"}
            onSubmit={addNewList}
            buttonText={"+ Add New List"}
            placeholder="Add new List"
          ></FormInput>
        </Box>
      </view>
    </>
  );
}

export default ListScreen;
