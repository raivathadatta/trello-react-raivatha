import { Box, Container, Typography } from "@mui/material";
import { getBoards } from "../data/getData/get_api_calls";
import { useEffect, useState } from "react";
import CircularProgress from "@mui/material/CircularProgress";
import CreateBoardDialog from "./components/dialogs/CreateBoardDialog";
import { createNewBoard } from "../data/create/create_api_calls";
import { Link } from "react-router-dom";
import ErrorSnackBar from "./components/snackbar/ErrorSnackBar";

function BoardsScreen() {
  const [board, setBoard] = useState([]);
  const [loading, setLoading] = useState(true);
  const [errorMessage, setErrorMessage] = useState("");

  const handleError = (message) => {
    setErrorMessage(message);
  };

  useEffect(() => {
    const getData = async () => {
      const response = await getBoards();
      if (response.error) {
        setLoading(false);
        handleError(response.error);
        return;
      }
      setBoard(response.data);
      setLoading(false);
    };
    getData();
  }, []);

  const createBoard = async (boardName) => {
    if (boardName.length < 2) {
      return;
    }

    let value = await createNewBoard(boardName);

    if (value.error) {
      console.log(value.error.message);
      setLoading(false);
      handleError(value.error.message);
    }

    setBoard([...data, value.data]);
  };

  if (errorMessage) {
    return (
      <ErrorSnackBar
        errorMessage={errorMessage}
        onCloseError={() => setErrorMessage("")}
      ></ErrorSnackBar>
    );
  }

  if (loading) {
    return (
      <Container sx={{ marginTop: "20%", marginLeft: "45%" }}>
        <CircularProgress />;
      </Container>
    );
  }
  return (
    <>
      <Container
        sx={{
          width: "90%",
          display: "flex",
          flexDirection: "row",
          flexWrap: "wrap",
        }}
      >
        <Box sx={{ width: "15%", margin: "10px" }}>
          <CreateBoardDialog createNewBoard={createBoard} />
        </Box>
        {board.length > 0 ? (
          board.map((board) => {
            return (
              <Link to={`/${board.id}`} key={board.id}>
                <Box
                  key={board.id}
                  sx={{
                    display: "flex",
                    width: "200px",
                    justifyContent: "flex-start",
                    alignItems: "baseline",
                    height: "100px",
                    border: "1px solid white",
                    borderRadius: "5px",

                    backgroundColor: "primary.main",
                    margin: 1,

                    boxShadow: 10,
                  }}
                >
                  <Typography
                    variant="body1"
                    sx={{
                      color: "white",
                      fontWeight: 700,
                      textTransform: "none",
                      padding: "5%",
                    }}
                  >
                    {board.name}
                  </Typography>
                </Box>
              </Link>
            );
          })
        ) : (
          <Typography
            sx={{ fontSize: "30px", textAlign: "center", color: "black" }}
          >
            There are no Boards
          </Typography>
        )}
      </Container>
    </>
  );
}

export default BoardsScreen;
